# -*- coding: utf-8 -*-

import dnsconf

zone = dnsconf.Zone('example.com.', 'root.example.com.')
root, _ = zone.add(
    dnsconf.ARecord(value='192.168.0.1'),
    dnsconf.AAAARecord(value='dead:beef::1'),
)

workstation, _ = zone.add(
    dnsconf.ARecord(name='workstation', value='192.168.0.2'),
    dnsconf.AAAARecord(name='workstation', value='dead:beef::2'),
)

zone.add(
    dnsconf.CNAMERecord(name='www', value=workstation),

    dnsconf.MXRecord(name='mail', priority=10, value=workstation),
    dnsconf.MXRecord(name='mail', priority=20, value=root),
)

print unicode(zone)
