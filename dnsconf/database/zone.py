# -*- coding: utf-8 -*-



class Zone(object):

    def __init__(self, root, owner,
        refresh=10800, # 3 hours
        retry=3600,    # 1 hour
        expire=604800, # 1 week
        minimum=36000  # 10 hours
    ):
        self.root = root
        self.owner = owner
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum
        self.records = []

    def add(self, *records):
        for record in records:
            if record.zone is not None:
                logging.warning('Adding a record to more than one zone.')
            record.zone = self
            self.records.append(record)
        return records

    def __unicode__(self):
        return u'Zone "%s" (%s):%s' % (
            self.root, self.owner,
            u''.join(
                u'\n\t%s' % record
                for record in self.records
            )
        )
