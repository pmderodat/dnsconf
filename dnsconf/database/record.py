# -*- coding: utf-8 -*-

__all__ = [
    'Record',
    'ARecord', 'AAAARecord',
    'CNAMERecord',
    'MXRecord',
]

class Record(object):

    def __init__(self, **kwargs):
        try:
            self.class_ = kwargs.pop('class_', u'IN')
        except KeyError:
            self.class_ = kwargs.pop('class', u'IN')
        self.type = kwargs.pop('type')
        # When no name is given, the zone root is meant.
        self.name = kwargs.pop('name', None)
        self.value = kwargs.pop('value')

        self.zone = None

    @property
    def fqdn(self):
        if self.zone is None:
            raise ValueError('Cannot get the FQDN of an unbound record')
        if self.name:
            return '%s.%s' % (self.name, self.zone.root)
        else:
            return self.zone.root

    def _unicode_value(self):
        if isinstance(self.value, Record):
            return self.value.fqdn
        else:
            return self.value

    def __unicode__(self):
        return u'%s[%s:%s]: %s' % (
            self.name or '', self.class_, self.type,
            self._unicode_value()
        )

class ARecord(Record):

    def __init__(self, **kwargs):
        kwargs['type'] = u'A'
        super(ARecord, self).__init__(**kwargs)

class AAAARecord(Record):
    def __init__(self, **kwargs):
        kwargs['type'] = u'AAAA'
        super(AAAARecord, self).__init__(**kwargs)

class CNAMERecord(Record):
    def __init__(self, **kwargs):
        kwargs['type'] = u'CNAME'
        super(CNAMERecord, self).__init__(**kwargs)

class MXRecord(Record):
    def __init__(self, **kwargs):
        self.priority = kwargs.pop('priority')
        kwargs['type'] = u'MX'
        super(MXRecord, self).__init__(**kwargs)

    def _unicode_value(self):
        return '%s %s' % (
            self.priority,
            super(MXRecord, self)._unicode_value()
        )
